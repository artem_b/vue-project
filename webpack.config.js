var DEBUG = process.env.NODE_ENV !== 'production' ? true : false;

var webpack = require('webpack');
var path = require('path');
var BundleTracker = require('webpack-bundle-tracker');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	context: __dirname,
	entry: './src/assets/src/main.js',
	output: {
		path: './src/assets/dist/',
		filename: '[name]-[hash].js',
	},
	module: {
		loaders: [
			{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader'),
			},
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader!postcss-loader'),
			},
			{
				test: /\.(png|jpg)$/,
				loader: 'url-loader?limit=32768',
			},
		]
	},
	plugins: [
		new BundleTracker({filename: './src/assets/webpack-stats.json'}),
		new ExtractTextPlugin('[name]-[hash].css')
	]
}