var Vue = require('vue');
var resource = require('vue-resource');

Vue.config.delimiters = ['{$', '$}'];
Vue.config.unsafeDelimiters = ['{!!', '!!}'];

Vue.use(resource);

new Vue({

	el: 'body',
	data: {
		event:{
			name: '',
			description: ''
		},
		events: [],
	},
	ready: function() {
		this.fetchEvent();
	},

	methods: {

	    fetchEvent: function () {
			var events = [];
			var self = this;
			this.$http.get('/api/events/?format=json')
				.then(function (res) {
					self.$set('events', res.data.results);
				})
				.catch(function (err) {
					console.log(err);
				});
	    },
		addEvent: function() {
			var self = this;
			this.$http.post('/api/events/?format=json', self.event)
				.then(function (res) {
					self.event = {
						name: '',
						description: '',
					}
					self.fetchEvent();
				})
				.catch(function (err) {
					console.log(err);
				});
		},
		deleteEvent: function(pk) {
			var self = this;
			this.$http.delete('/api/events/'+pk+'/?format=json')
				.then(function (res) {
					self.fetchEvent();
				})
				.catch(function (err) {
					console.log(err);
				});
		}
	}
});