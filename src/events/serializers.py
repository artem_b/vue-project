# -*- coding: utf-8 -*-
from rest_framework import serializers

from events.models import Event

class EventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Event
        fields = (
        	'pk',
            'name', 
            'description', 
            'date',
        )