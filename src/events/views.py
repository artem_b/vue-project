# -*- coding: utf-8 -*-
from django.shortcuts import render
from rest_framework import viewsets

from events.models import Event
from events.serializers import EventSerializer


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all().order_by('date')
    serializer_class = EventSerializer