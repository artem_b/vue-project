# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Event(models.Model):
    name = models.CharField(
        max_length=200,
        verbose_name=u'title',
    )
    description = models.TextField(
        verbose_name=u'description',
    )
    date = models.DateField(
        verbose_name=u'date',
        auto_now=True,
    )

    class Meta:
        verbose_name = u'event'
        verbose_name_plural = u'events'

    def __unicode__(self):
        return self.name