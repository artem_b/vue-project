from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from events.views import EventViewSet
from django.views.generic import TemplateView


router = routers.DefaultRouter()
router.register(r'events', EventViewSet)

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="base.html"), name='base'),
    url(r'^api/', include(router.urls)),
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
